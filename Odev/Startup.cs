﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Odev.Startup))]
namespace Odev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
