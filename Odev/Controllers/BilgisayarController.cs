﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Odev.Models;

namespace Odev.Controllers
{
    public class BilgisayarController : Controller
    {
        RSLNREntities db = new RSLNREntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Notebooklar()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 1).ToList();
            return View(liste);
        }

        public ActionResult OyunBilgisayarlari()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 2).ToList();
            return View(liste);
        }

        public ActionResult Ikisi1AradaBilgisayarlar()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 3).ToList();
            return View(liste);
        }

        public ActionResult AllInOneBilgisayarlar()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 4).ToList();
            return View(liste);
        }

    }
}