﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Odev.Models;

namespace Odev.Controllers
{
    public class AnaSayfaController : Controller
    {
        RSLNREntities db = new RSLNREntities();
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult _Slider()
        {
            var liste = db.Slider.Where(x=>x.BaslangicTarihi<DateTime.Now && x.BitisTarihi >DateTime.Now).OrderByDescending(x => x.SliderID);
            return View(liste);
        }

        public ActionResult _BilgisayarSlider()
        {
            var liste = db.Urunler.Where(x=>x.KategoriID <=4).OrderByDescending(x=>x.UrunID);
            return View(liste);
        }

        public ActionResult _TelefonSlider()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 5).OrderByDescending(x => x.UrunID);
            return View(liste);
        }

        public ActionResult _SesMuzikSlider()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 6).OrderByDescending(x => x.UrunID);
            return View(liste);
        }

    }
}