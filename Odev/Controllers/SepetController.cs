﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Odev.Models;
using Microsoft.AspNet.Identity;


namespace Odev.Controllers
{
    public class SepetController : Controller
    {
        RSLNREntities db = new RSLNREntities();
        
        // GET: Sepet
        public ActionResult Index()
        {
            string user = User.Identity.GetUserId();
            var model = db.Sepet.Where(x => x.SepetSahibi == user).ToList();
            return View(model);
        }

        public ActionResult SepeteEkle(int id)
        {
            var bulunan = db.Urunler.Find(id);
            Sepet sepet = new Sepet();
            sepet.SepetSahibi = User.Identity.GetUserId();
            sepet.UrunID = bulunan.UrunID;
            db.Sepet.Add(sepet);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult SepettenCikar(int id)
        {
            var model = db.Sepet.Find(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult SepettenCikarOnay(int id)
        {
            var model = db.Sepet.Find(id);
            db.Sepet.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}