﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Odev.Models;

namespace Odev.Controllers
{
    public class SesMuzikController : Controller
    {
        RSLNREntities db = new RSLNREntities();
        // GET: SesMuzik
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SesSistemleri()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 6);
            return View(liste);
        }

        public ActionResult DigerSesAygitlari()
        {
            var liste = db.Urunler.Where(x => x.KategoriID == 7);
            return View(liste);
        }

       
    }
}