﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Odev.Areas.Admin.Models
{
    public class AdminUrun
    {

      
        public AdminUrun()
        {
            this.UrunFotograflari = new HashSet<UrunFotograflari>();
        }

        public int UrunID { get; set; }
        public int KategoriID { get; set; }
        public int MarkaID { get; set; }
        public string UrunFotoYol { get; set; }
        public string UrunModel { get; set; }
        public string UrunOzellikler { get; set; }
        public Nullable<double> UrunFiyat { get; set; }
        public HttpPostedFileBase Resim { get; set; }
        public virtual Kategori Kategori { get; set; }
        public virtual Markalar Markalar { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Sepet> Sepet { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UrunFotograflari> UrunFotograflari { get; set; }

        public static class DropdownHelper
        {
            public static IEnumerable<SelectListItem> KategorilerCek()
            {
                RSLNREntities db = new RSLNREntities();
                return db.Kategori.Select(x => new SelectListItem { Text = x.KategoriAdi, Value = x.KategoriID.ToString() });
            }

            public static IEnumerable<SelectListItem> MarkalariCek()
            {
                RSLNREntities db = new RSLNREntities();
                return db.Markalar.Select(x => new SelectListItem { Text = x.MarkaAd, Value = x.MarkaID.ToString() });
            }
        }
    }
}