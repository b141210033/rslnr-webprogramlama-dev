﻿using Odev.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Odev.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UrunController : Controller
    {
        RSLNREntities db = new RSLNREntities();
        // GET: Admin/Urun
        public ActionResult Index()
        {
            var model = db.Urunler.OrderByDescending(x => x.UrunID).ToList();
            return View(model);
        }


        public ActionResult Ekle()
        {
            return View();
        }

        public ActionResult UruneEkle(AdminUrun model)
        {
            string dosyaadi = String.Empty;
            if (model.Resim != null && model.Resim.ContentLength > 0)
            {
                dosyaadi = model.Resim.FileName;
                var yol = Path.Combine(Server.MapPath("/Content/images/Urunler/"), dosyaadi);
                model.Resim.SaveAs(yol);


                Urunler urun = new Urunler();
                urun.KategoriID = model.Kategori.KategoriID;
                urun.MarkaID = model.Markalar.MarkaID;
                urun.UrunModel = model.UrunModel;
                urun.UrunOzellikler = model.UrunOzellikler;
                urun.UrunFiyat = model.UrunFiyat;
                urun.UrunFotoYol = "/Content/images/Urunler/" + dosyaadi;


                db.Urunler.Add(urun);
                db.SaveChanges();
            }


            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Duzenle(int id)
        {

            var model = db.Urunler.Find(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Duzenle(Urunler model)
        {

            if (ModelState.IsValid)
            {
                var bulunan = db.Urunler.Find(model.UrunID);
                bulunan.KategoriID = model.KategoriID;
                bulunan.MarkaID = model.MarkaID;
                bulunan.UrunFiyat = model.UrunFiyat;
                bulunan.UrunFotoYol = model.UrunFotoYol;
                bulunan.UrunModel = model.UrunModel;
                bulunan.UrunOzellikler = model.UrunOzellikler;
                
                db.SaveChanges();

            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Sil(int id)
        {
            var model = db.Urunler.Find(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult SilOnay(int id)
        {
            var model = db.Urunler.Find(id);
            db.Urunler.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }



}