﻿using Odev.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Odev.Models;

namespace Odev.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SliderController : Controller
    {
        RSLNREntities db = new RSLNREntities();
        // GET: Admin/Slider
        public ActionResult Index()
        {
            var model = db.Slider.OrderByDescending(x => x.SliderID).ToList();
            return View(model);       
        }

        public ActionResult Ekle()
        {
            return View();
        }

        public ActionResult SlideraEkle(SliderModel model)
        {
            string dosyaadi = String.Empty;
            if (model.Resim != null && model.Resim.ContentLength > 0)
            {
                dosyaadi = model.Resim.FileName;
                var yol = Path.Combine(Server.MapPath("/Content/images/slide/"), dosyaadi);
                model.Resim.SaveAs(yol);



                Slider slider = new Slider();
                slider.BaslangicTarihi = model.BaslangicTarihi;
                slider.BitisTarihi = model.BitisTarihi;
                slider.ResimYolu = "/Content/images/slide/" + dosyaadi;

                db.Slider.Add(slider);
                db.SaveChanges();
            }


            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Duzenle(int id)
        {
            var model = db.Slider.Find(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Duzenle(Slider model)
        {
            if(ModelState.IsValid)
            {
                db.Slider.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Sil(int id)
        {
            var model = db.Slider.Find(id);
            return View(model);
        }

        [HttpGet]
        public ActionResult SilOnay(int id)
        {
            var model = db.Slider.Find(id);
            db.Slider.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        


    }

}