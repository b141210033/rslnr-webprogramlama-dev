﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Odev.Models
{
    public class SepetModel
    {
        public int SepetID { get; set; }
        public Nullable<int> UrunID { get; set; }
        public string SepetSahibi { get; set; }

        public virtual Urunler Urunler { get; set; }
    }
}